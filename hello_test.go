package main

import (
	"testing"
)

func TestHello(t *testing.T) {
	want := "hello"
	if get := hello(); get != want {
		t.Fatalf("want:%s get:%s", want, get)
	}
}
